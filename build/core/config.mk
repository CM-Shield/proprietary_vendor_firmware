ifneq ($(wildcard vendor/firmware/$(TARGET_DEVICE)/config.mk),)
include vendor/firmware/$(TARGET_DEVICE)/config.mk
else ifneq ($(wildcard vendor/firmware/$(TARGET_REFERENCE_DEVICE)/config.mk),)
include vendor/firmware/$(TARGET_REFERENCE_DEVICE)/config.mk
endif
